from django.shortcuts import render, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm


# Create your views here.
@login_required
def project_list(request):
    project_all = Project.objects.filter(owner=request.user)
    context = {
        "list_projects": project_all,
    }
    return render(request, "projects/list.html", context)


@login_required
def project_detail(request, id):
    list = Project.objects.get(id=id)
    context = {
        "list": list,
    }
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)
